#ifndef _GBASIC_STRING_UTIL_H
#define _GBASIC_STRING_UTIL_H

/**
 * Preconditions:
 * After: The returned string should be appropriately
 * freed by the receiver.  Does nothing now...
 */
char**
string_util_split_at_whitespace(char *line);

void
string_util_strip_excess_whitespace(char *line);

int
string_util_get_length(char *line);

int
string_util_count_words(char *line);

#endif