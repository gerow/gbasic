#include <string_util.h>

#include <stdlib.h>

// TODO
char**
string_util_split_at_spaces(char *line)
{
	int i;
	int j;
	int num;
	int word_num = 0;
	char** out;
	char c;

	num = string_util_count_words(line);

	out = (char**)malloc(num * sizeof(char*));

	while ((c = line[i]) != '\0') {
		if (c == ' ') {
			continue;
		}

		++word_num;
	}
}

void
string_util_strip_excess_whitespace(char *line)
{
	char* temp;
	int i = 0;
	int temp_index = 0;

	temp = malloc((string_util_get_length(line) + 1) * sizeof(char));

	//Remove all whitespace from the beginning

	while(line[i] != '\0') {
		if (line[i] == ' ') {
			while ((line[i] != '\0') && (line[i] == ' '));
		} else {
			temp[temp_index] = line[i];
			temp_index++;
		}
		i++;
	}

	temp[temp_index] = '\0';

	i = 0;
	while (temp[i] != '\0') {
		line[i] = temp[i];
		i++;
	}
	line[i] = '\0';

	free(temp);
}

int
string_util_get_length(char *line)
{
	int i = 0;
	int num = 0;

	while (line[i] != '\0') {
		num++;
	}

	return num;
}

int
string_util_count_words(char *line)
{
	int num = 1;
	int i = 0;
	int hit_space = 0;
	char c;

	while ((c = line[i]) != '\0') {
		if (c == ' ') {
			if (!hit_space) {
				num++;
			}
			hit_space = 1;
		} else {
			hit_space = 0;
		}

		i++;
	}

	return num;
}